import java.util.Stack;
/**
 * Write a description of class EvaluatePostFix here.
 * 
 * Trevor Beall 
 * 10-19-15
 */
public class EvaluatePostfix
{
    // instance variables - replace the example below with your own
    
    Stack operands;

    /**
     * Constructor for objects of class EvaluatePostFix
     */
    public EvaluatePostfix()
    {
        operands = new Stack();
    }

    
    public double evaluate(String postfix)
    {
        String[] split = postfix.split(" +");
        double operand1, operand2;
        for(int i = 0; i < split.length; i++)
        {
            if(!isOperator(split[i]))
            {
                operands.push(Double.parseDouble(split[i]));
            }
            else
            {
                operand2 = (Double)operands.pop();
                operand1 = (Double)operands.pop();
                switch (split[i])
                {
                    case "+": operands.push(operand1 + operand2); break;
                    case "-": operands.push(operand1 - operand2); break;
                    case "/": operands.push(operand1 / operand2); break;
                    case "*": operands.push(operand1 * operand2); break;
                    case "^": operands.push(Math.pow(operand1, operand2)); break;
                    default: break;
                }
            }
        }
        return (Double)operands.pop();
    }
    
    public boolean isOperator(String c)
    {
        if ( c.equals("+") ||
             c.equals("/") ||
             c.equals("*") ||
             c.equals("-") ||
             c.equals("^"))
            return true;
        else
            return false;
    }
}
