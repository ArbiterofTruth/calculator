

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class InfixToPostfixTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class InfixToPostfixTest
{
    /**
     * Default constructor for test class InfixToPostfixTest
     */
    public InfixToPostfixTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testAddSub()
    {
        InfixToPostfix test = new InfixToPostfix();
        assertEquals("1 2 +", test.toPostfix("1 + 2"));
        assertEquals("2 1 -", test.toPostfix("2 - 1"));
    }
    
    @Test
    public void testMultDiv()
    {
        InfixToPostfix test = new InfixToPostfix();
        assertEquals("2 4 *", test.toPostfix("2 * 4"));
        assertEquals("4 2 /", test.toPostfix("4 / 2"));
    }
    
    @Test
    public void testMultAdd()
    {
        InfixToPostfix test = new InfixToPostfix();
        assertEquals("A B * C +", test.toPostfix("A * B + C"));
        assertEquals("A B C * +", test.toPostfix("A + B * C"));
        assertEquals("A B C * D / + E 2 * -", test.toPostfix("A + B * C / D - E * 2"));
    }
    
    @Test
    public void testAssociation()
    {
        InfixToPostfix test = new InfixToPostfix();
        assertEquals("A B - C +", test.toPostfix("A - B + C"));
        assertEquals("A B * C / D * E *", test.toPostfix("A * B / C * D * E"));
    }
    
    @Test
    public void testParenthenses()
    {
        InfixToPostfix test = new InfixToPostfix();
        assertEquals("2 2 1 + *", test.toPostfix("2 * ( 2 + 1 )"));
    }
}
