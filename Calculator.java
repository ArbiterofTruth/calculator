import acm.program.*;
import acm.gui.*;
import javax.swing.*;
import java.awt.Dimension;
import java.awt.event.*;
import java.awt.Color;


public class Calculator extends Program
{
    private JTextField infix, result;
    private JButton goButton, clearButton, space, zero, one, two, three, four, five, six, seven, eight, nine, plus, minus, divide, multiply, power, leftParentheses, rightParentheses;
    
    public Calculator()
    {
        start();
        setSize(600, 400);
        setBackground(Color.BLUE);
    }
    
    public void init()
    {
        TableLayout Calc = new TableLayout(12, 2);
        setLayout(Calc);

        infix = new JTextField();
        result = new JTextField();
        
        infix.setEditable(false);
        result.setEditable(false);
        
        add(infix);
        add(result);
        
        space = new JButton("_");
        space.setActionCommand("_");
        zero = new JButton("0");
        zero.setActionCommand("0");
        one = new JButton("1");
        one.setActionCommand("1");
        two = new JButton("2");
        two.setActionCommand("2");
        three = new JButton("3");
        three.setActionCommand("3");
        four = new JButton("4");
        four.setActionCommand("4");
        five = new JButton("5");
        five.setActionCommand("5");
        six = new JButton("6");
        six.setActionCommand("6");
        seven = new JButton("7");
        seven.setActionCommand("7");
        eight = new JButton("8");
        eight.setActionCommand("8");
        nine = new JButton("9");
        nine.setActionCommand("9");
        plus = new JButton("+");
        plus.setActionCommand("+");
        minus = new JButton("-");
        minus.setActionCommand("-");
        multiply = new JButton("*");
        multiply.setActionCommand("*");
        divide = new JButton("/");
        divide.setActionCommand("/");
        power = new JButton("^");
        power.setActionCommand("^");
        leftParentheses = new JButton("(");
        leftParentheses.setActionCommand("(");
        rightParentheses = new JButton(")");
        rightParentheses.setActionCommand(")");
        goButton = new JButton("Go!");
        goButton.setActionCommand("Go!");
        clearButton = new JButton("Clear");
        clearButton.setActionCommand("Clear");
        
        add(goButton);
        add(clearButton);
        add(nine);
        add(eight);
        add(seven);
        add(six);
        add(five);
        add(four);
        add(three);
        add(two);
        add(one);
        add(zero);
        add(leftParentheses);
        add(rightParentheses);
        add(space);
        add(power);
        add(plus);
        add(minus);
        add(multiply);
        add(divide);
        
        addActionListeners();
    }
    
    public void actionPerformed(ActionEvent ae)
    {
        String infixString = infix.getText();
        InfixToPostfix postfixExp = new InfixToPostfix();
        double expression = 0;
        EvaluatePostfix eval = new EvaluatePostfix();
        String postfixString = "";
        
        String what = ae.getActionCommand();
        if (what.equals("Clear"))
        {
            infix.setText("");
            result.setText("");
        }
        else if (what.equals("Go!"))
        {
            postfixString = postfixExp.toPostfix(infixString);
            expression = eval.evaluate(postfixString);
            result.setText("" + expression);
        }
        else
        {
            infixString = infixString + value(what);
            infix.setText("" + infixString);
        }
    }
    
    public String value(String c)
    {
        switch (c)
        {
            case "0": return "0";
            case "1": return "1";
            case "2": return "2";
            case "3": return "3";
            case "4": return "4";
            case "5": return "5";
            case "6": return "6";
            case "7": return "7";
            case "8": return "8";
            case "9": return "9";
            case "_": return " ";
            case "^": return "^";
            case "+": return "+";
            case "-": return "-";
            case "*": return "*";
            case "/": return "/";
            case "(": return "(";
            case ")": return ")";
            default: return "";
        }
    }
}