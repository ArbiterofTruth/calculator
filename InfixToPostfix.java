import java.util.Stack;
/**
 * Write a description of class InfixToPostfix here.
 * 
 * Trevor Beall
 * 10/11/2015
 */
public class InfixToPostfix
{
    Stack operators = new Stack();

    /**
     * Constructor for objects of class InfixToPostfix
     */
    public InfixToPostfix()
    {
        
    }

    /**
     * toPostfix
     */
    public String toPostfix(String infix)
    {
        String[] tokens = infix.split(" +");;
        int i;
        int length = tokens.length;
        String operator;
        String output = "";
        
        for (i = 0; i < length; i++)
        {
            if (isOperator(tokens[i]))
            {
                if (operators.empty() || tokens[i].equals("(") || operators.peek().equals("("))
                    operators.push(tokens[i]);
                else
                {
                    if(operatorLessPrecedence(tokens[i]))
                    {
                        if (tokens[i].equals(")")) 
                        {
                            do
                            {
                                operator = (String)operators.pop();
                                if(!operator.equals("("))
                                output = output + " " + operator;
                            }
                            while (!operators.empty() && !operator.equals("("));
                            
                        }
                        else
                        {
                            output =output + " "+ operators.pop();
                            while(!operators.empty() && operatorLessPrecedence(tokens[i]))
                            {
                                output = output + " " + operators.pop();
                            }
                            operators.push(tokens[i]);
                        }
                    }
                    else
                    {
                        operators.push(tokens[i]);
                    }
                }
            }
            else
            {
                if(output.length() == 0)
                    output = output + tokens[i];
                else
                    output = output + " " + tokens[i];
            }
        }
        while (!operators.empty())
        {
            // 5. At the end of the expression, pop and print all operators on the stack.
            operator = (String)operators.pop();
            if(!operator.equals("("))
                output = output + " " + operator;
        }
        return output;
    }
    
    /**
     * isOperator
     */
    public boolean isOperator(String c)
    {
        if ( c.equals("+") ||
             c.equals("/") ||
             c.equals("*") ||
             c.equals("-") ||
             c.equals("^") ||
             c.equals("(") ||
             c.equals(")"))
            return true;
        else
            return false;
    }
    
    /**
     * operatorLessPrecedence
     * Compare operator with top of stack
     * Assume association left to right
     */
    public boolean operatorLessPrecedence(String o)
    {
        int operatorPrecedence = precedence(o);
        int tosPrecedence = precedence((String)operators.peek());
        return (operatorPrecedence <= tosPrecedence);
    }
    
    /**
     * precedence
     */
    public int precedence(String o)
    {
        switch (o)
        {
            case ")": return 0;
            case "+": return 1;
            case "-": return 1;
            case "*": return 2;
            case "/": return 2;
            case "^": return 3;
            case "(": return 4;
        }
        return 5;
    }
}
